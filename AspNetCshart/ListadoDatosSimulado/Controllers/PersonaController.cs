﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ListadoDatosSimulado.Models;

namespace ListadoDatosSimulado.Controllers
{
    public class PersonaController : Controller
    {
        Random ran = new Random();

        List<Persona> lista = new List<Persona>();
        // GET: Persona
        public ActionResult Index()
        {
            for(int x=0; x<10; x++)
            {
                ListadoPersonas();
            }

            return View(lista);
        }

        public void ListadoPersonas()
        {
            Persona per = new Persona();
            string[] nombres = new string[] { "Vicente", "Ignacio", "Dante", "Arial", "Sonia", "Yesmenia" };
            string[] aMater = new string[] { "Garate", "Piñeda", "Rifo", "Vergara", "Parez", "Gonzalez" };
            string[] aPater = new string[] { "Garate", "Piñeda", "Rifo", "Vergara", "Perez", "Gonzalez" };

            per.codigo = ran.Next(1, 20);
            per.nombre = nombres[ran.Next(0, nombres.Length)];
            per.aMaterno = aMater[ran.Next(0, aMater.Length)];
            per.aPaterno = aPater[ran.Next(0, aPater.Length)];

            lista.Add(per);

        }
    }
}