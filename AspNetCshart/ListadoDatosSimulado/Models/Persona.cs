﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ListadoDatosSimulado.Models
{
    public class Persona
    {
        public int codigo { get; set; }
        public string nombre { get; set; }
        public string aPaterno { get; set; }    
        public string aMaterno { get; set; }
    }
}